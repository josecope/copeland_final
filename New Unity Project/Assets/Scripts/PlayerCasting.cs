﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCasting : MonoBehaviour
{
    public static float DistanceFromTarget; //static allows it to be referenced by other scripts--- this line is setting up how far away objects in the environment are
    public float ToTarget;

    // Update is called once per frame
    void Update()
    {
        RaycastHit Hit;
        if (Physics.Raycast (transform.position, transform.TransformDirection(Vector3.forward), out Hit)) //once the raycast is fired, hit the target and set the distance from it
        {
            ToTarget = Hit.distance; //totarget is going to be equal to the distance
            DistanceFromTarget = ToTarget;
        }
    }
}
