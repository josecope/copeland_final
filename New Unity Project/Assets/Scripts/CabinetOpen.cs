﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//essentially the same as the door opening script! just changed for for the new object!
public class CabinetOpen : MonoBehaviour
{
    public float TheDistance; //will allow us to reference the PlayerCasting script
    public GameObject ActionDisplay;//public variable that will let us reference an object as the action display, and also another object as the action text and as the door in the next line
    public GameObject ActionText;
    public GameObject TheCabinet;
    public AudioSource CreakSound;

    // Update is called once per frame
    void Update()
    {
        TheDistance = PlayerCasting.DistanceFromTarget; //references the variable from the other script and sets them equal
    }

    private void OnMouseOver()
    {
        if (TheDistance <= 2)
        {
            ActionDisplay.SetActive(true);
            ActionText.SetActive(true);
        }

        if (Input.GetButtonDown("Action"))
        {
            //nested if function
            if (TheDistance <= 2)
            { //Shut off the mesh collider, turn off the UI displays, play the door animation by getting the component from TheCabinet, and play the CreakSound audio
                this.GetComponentInChildren<MeshCollider>().enabled = false;
                ActionDisplay.SetActive(false);
                ActionText.SetActive(false);
                TheCabinet.GetComponent<Animation>().Play("CabinetSwing");
                CreakSound.Play();
            }
        }
    }

    private void OnMouseExit()
    {
        ActionDisplay.SetActive(false);
        ActionText.SetActive(false);
    }
}
