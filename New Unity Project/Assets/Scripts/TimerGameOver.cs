﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TimerGameOver : MonoBehaviour
{
    int countDownStartValue = 100; //where the timer will start from (seconds)

    public Text timerUI;

    public GameObject GameOver;

    public GameObject Win;
    public GameObject YouWin;

    public GameObject Player;
    
    // Start is called before the first frame update
    void Start()
    {
        countDownTimer(); //call the function so that it activates when the game starts
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == Player)
        {
            if (!YouWin.activeSelf) YouWin.SetActive(true);
            Win.SetActive(false);
        }
    }


    void countDownTimer()
    {
        if (countDownStartValue > 0)
        {
            TimeSpan spanTime = TimeSpan.FromSeconds(countDownStartValue);
            timerUI.text = "Escape! " + spanTime.Minutes + " : " + spanTime.Seconds;
            countDownStartValue--; //decrease the timer by one
            Invoke("countDownTimer", 1.0f); //call the method again after the second is subtracted, until the value is 0
        }


        else
        {
            if (!GameOver.activeSelf) GameOver.SetActive(true);
            //game over condition when the count down is not greater than 0
            //set game over game object to be active
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
