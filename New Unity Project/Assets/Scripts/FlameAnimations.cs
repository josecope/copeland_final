﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Tells the script what to reference to know what is going on

public class FlameAnimations : MonoBehaviour
{
    //which animation to use
    public int LightMode;

    public GameObject FlameLight;


    // Start is called before the first frame update, meaning that it only runs once
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (LightMode == 0) //if lightmode is 0, do something
        {

            StartCoroutine(AnimateLight()); //tells the script to run the AnimateLight sequence

        }



    }

    IEnumerator AnimateLight()
    {

        LightMode = Random.Range(1, 4);
        if (LightMode == 1)
        {

            FlameLight.GetComponent<Animation>().Play("TorchAnim1");

        }

        if (LightMode == 2)
        {

            FlameLight.GetComponent<Animation>().Play("TorchAnim2");

        }

        if (LightMode == 3)
        {

            FlameLight.GetComponent<Animation>().Play("TorchAnim3");

        }

        if (LightMode == 1)
        {

            FlameLight.GetComponent<Animation>().Play("TorchAnim1");

        }

        yield return new WaitForSeconds(0.99f); //commands the script to wait for just under a second

        LightMode = 0; //allows the routine to be run again
    }
}
